import React from 'react';
import { useFetch } from "./hooks";

function AppInitial() {
  const [data, loading] = useFetch("https://gosquared-api.netlify.com/.netlify/functions/api");
  const {visitors, officeTempCelsius, nextPlantWatering, weather, drinksLeft, nextBDay, nextHoliday} = data;
  return (
    <>
      {console.log(data)}
      {loading ? (
        "Loading..."
      ) : (
        <div className="grid-container view-1">
          <div>
            <h2>Active Visitors</h2>
            {visitors}
          </div>
          <div>
            <h2>Office Temperature</h2>
            {officeTempCelsius}
          </div>
          <div className="grid-item-highlight">
            <h2>Weather</h2>
            <strong>Forecast</strong> {weather.forecast}<br/>
            <strong>Humidity</strong> {weather.humidityPercentage}<br/>
            <strong>Wind</strong> {weather.windKmH}
          </div>
          <div>
            <h2>Next watering Plant</h2>
            {nextPlantWatering}
          </div>
          <div className="grid-item-highlight">
            <h2>Drinks Left</h2>
            <strong>CokeZero</strong> {drinksLeft.cokeZero}<br/>
            <strong>Orange Juice</strong> {drinksLeft.orangeJuice}<br/>
            <strong>Sprite</strong> {drinksLeft.sprite}
          </div>
          <div>
            <h2>Next BDay</h2>
            {nextBDay.name}<br/>
            {nextBDay.date}<br/>
            {nextBDay.email}
          </div>
          <div>
            <h2>Next Holiday</h2>
            {nextHoliday}
          </div>
        </div>
        // <div>yay</div>
        
        // <ul>
        //   {data.map(({ visitors, officeTempCelsius, nextBDay }) => (
        //     <li key={`photo-${visitors}`}>
        //       <img alt={officeTempCelsius} src={nextBDay.pic} />
        //     </li>
        //   ))}
        // </ul>
      )}
    </>
  );
}

export default AppInitial;
