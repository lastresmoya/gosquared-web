import React, {Component} from 'react';
import AOS from 'aos'; 

class GridItem extends Component {
  constructor(props, context) { 
    super(props, context); 
    AOS.init(); 
  } 
  componentWillReceiveProps (){ 
    AOS.refresh(); 
  } 
  
  render(){
    return (
      <div 
        className={`grid-item grid-item-${this.props.type} ${this.props.active && 'active'}`} 
        data-aos="fade-up"
        data-aos-delay={Math.floor(Math.random()*6)*100}
        >
        
        {this.props.type !== 'highlight' && 
          <div className="grid-item-body">
            <div className="grid-item-icon"><img src={this.props.icon} alt=""/></div>
            <div className="w-100">
              <h3 className="text-3">{this.props.title}</h3>
              <div className="text-1">
                {this.props.children}
              </div>
            </div>
          </div>
        }
        {this.props.type === 'highlight' && 
          <div className="w-100">
            <div className="d-flex justify-content-between align-items-center">
              <h3 className="text-2">{this.props.title}</h3> <div className="grid-item-icon"><img src={this.props.icon} alt="" /></div>
            </div>
            <div className="grid-item-body">{this.props.children}</div>
          </div>
        }
  
      </div>
    )
  }
}

export default GridItem;