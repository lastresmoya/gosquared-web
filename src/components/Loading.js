import React from 'react';

function Loading(props) {
  return (
    <div className="loading" data-aos>
      <span>Loading...</span>
    </div>
  )
}

export default Loading;