import React from 'react';
import { useFetch } from "./hooks";
import Loading from './components/Loading';
import logo from './assets/logo.png';
import GridItem from './components/GridItem';
import iconSoda from './assets/icon-soda.svg';
import iconPlant from './assets/icon-plant.svg'
import iconCake from './assets/icon-cake.svg'
import iconWeather from './assets/icon-weather.svg'
import iconCalendar from './assets/icon-calendar.svg'
import iconUser from './assets/icon-user.svg'


function AppTurbo() {
  const [data, loading] = useFetch("https://gosquared-api.netlify.com/.netlify/functions/api");
  const {visitors, officeTempCelsius, nextPlantWatering, weather, drinksLeft, nextBDay, nextHoliday} = data;


  return (
    <>
      {loading ? (
        <Loading/>
      ) : (
        <div className="view-turbo">
          <header>
            <h1 className="text-1 text-white">Office Overview</h1>
            <img src={logo} alt="" className="logo"/>
          </header>
          <div className="grid-container">
            <GridItem title="Beverages Left in Fridge" icon={iconSoda} type="default">
              {drinksLeft.cokeZero+drinksLeft.orangeJuice+drinksLeft.sprite} Cans
            </GridItem>
            <GridItem title="Weather Outside" icon={iconWeather} type="extended" active>
              {weather.forecast} | Humidity {weather.humidityPercentage}% | Wind:{weather.windKmH}km/h
            </GridItem>
            <GridItem title="Current Website Visitors" icon={iconUser} type="default">
              {visitors}
            </GridItem>
            <GridItem title="Office Temp" icon={iconWeather} type="default">
              {officeTempCelsius}°
            </GridItem>
            <GridItem title="Forecast" icon={iconWeather} type="extended">
              {weather.forecast}
            </GridItem>
            <GridItem title="Next Holiday" icon={iconCalendar} type="default">
              {nextHoliday}
            </GridItem>
            <GridItem title="Next Watering" type="extended" icon={iconPlant}>{nextPlantWatering}</GridItem>
            <GridItem title="Beverages in Fridge" icon={iconSoda} type="extended">
              <div className="grid-item-list text-3">
                <div><strong>Coke Zero</strong>  {drinksLeft.cokeZero}</div>
                <div><strong>Orange Juice</strong> {drinksLeft.orangeJuice}</div>
                <div><strong>Sprite</strong> {drinksLeft.sprite}</div>
              </div>
            </GridItem>
            <GridItem title="Next BDay" icon={iconCake} type="highlight">
              <div className="d-flex align-items-center justify-content-between w-100">
                <img src={nextBDay.pic} alt="" className="avatar"/>
                <div className="text-3">
                  {nextBDay.name}
                   {/* | {nextBDay.email} */}
                </div>
                {nextBDay.date}
              </div>
              
            </GridItem>

          </div>
        </div>
      )}
    </>
  );
}

export default AppTurbo;
